document.addEventListener('DOMContentLoaded', function() {
    const exploreButton = document.getElementById('exploreButton');
    const nextSection = document.getElementById('nextSection');

    exploreButton.addEventListener('click', function(e) {
        e.preventDefault();
        nextSection.scrollIntoView({
            behavior: 'smooth'
        });
    });
});