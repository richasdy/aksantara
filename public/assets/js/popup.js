document.addEventListener('DOMContentLoaded', function() {
    const popupTriggers = document.querySelectorAll('[data-popup]');
    
    popupTriggers.forEach(trigger => {
        trigger.addEventListener('click', function(e) {
            e.preventDefault();
            const popupId = this.getAttribute('data-popup');
            const popup = document.getElementById(popupId);
            if (popup) {
                popup.style.display = 'block';
            }
        });
    });

    const closeButtons = document.querySelectorAll('.close');

    closeButtons.forEach(button => {
        button.addEventListener('click', function(e) {
            e.stopPropagation(); 
            this.closest('.popup').style.display = 'none';
        });
    });

    window.addEventListener('click', function(event) {
        if (event.target.classList.contains('popup')) {
            event.stopPropagation();
            event.target.style.display = 'none';
        }
    });

    const popups = document.querySelectorAll('.popup');
    popups.forEach(popup => {
        popup.addEventListener('click', function(e) {
            e.stopPropagation(); 
        });
    });
});